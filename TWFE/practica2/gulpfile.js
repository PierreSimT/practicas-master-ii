var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var sass = require('gulp-ruby-sass');
var minifyCss = require('gulp-minify-css');
var sourcemaps = require('gulp-sourcemaps');
var imagemin = require('gulp-imagemin');

gulp.task('sass', function() {

	return sass('src/**/*.scss', {sourcemap: true})
		.pipe(sourcemaps.write('./', {
			includeContent: false,
			sourceRoot: './dist'
		}))
		.pipe(gulp.dest('dist'))
		.pipe(browserSync.stream({match: '**/*.css'}));
});

gulp.task('html', function() {
	return gulp.src('src/html/*.html')
			.pipe(gulp.dest('dist'));
});

gulp.task('javascript', function() {
	return gulp.src(['src/**/*.js'])
		.pipe(sourcemaps.init())
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('dist'));
});

gulp.task('img', function () {
	return gulp.src(['src/img/*.png', 'src/img/*.jpg'])
		.pipe(imagemin())
		.pipe(gulp.dest('dist/img'));
});

gulp.task('serve', function() {

	browserSync.init({
		server: {
			baseDir: 'dist/'
		}
	});

	gulp.watch('src/styles/*.scss', gulp.parallel('sass'));
	gulp.watch('src/scripts/*.js', gulp.parallel('javascript'));
	gulp.watch('src/img/*', gulp.parallel('img'));
	gulp.watch('src/html/*.html', gulp.parallel('html')).on('change', browserSync.reload);

});

gulp.task('default', gulp.series('sass', 'javascript', 'img', 'html', 'serve'));