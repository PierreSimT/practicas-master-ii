# Tecnologías Web: Cliente - Práctica 4

 El objetivo de la práctica 4 es realizar un modelo del esquema que se presenta a continuación, mediante el uso de web components.

 ![esquema](./doc/esquema.png)

 Para ello se ha implementado el componente central en su propia carpeta llamada `component/`. Ver contenido del archivo en JavaScript [aqui](component/fragment.js).

 A continuación se muestra la realización de la práctica en funcionamiento.

 ![gif](./doc/web.gif)