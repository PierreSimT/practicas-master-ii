(function fragments() {
    const data = {
        products: [{
            name: 'Product 1',
            desc: 'This is description for prod 1',
            url: '#',
        },
        {
            name: 'Product 2',
            desc: 'This is description for prod 2',
            url: '#',
        },
        {
            name: 'Product 3',
            desc: 'This is description for prod 3',
            url: '#',
        }],
    };

    class Central extends HTMLElement {
        connectedCallback() {
            this.updateData = this.updateData.bind(this);
            this.log('connected');
            this.render();
        }

        updateData(e) {
            this.log('event sent "central:click"');
            const $product = document.getElementById('selected-prod');
            $product.innerText = 'Se ha seleccionado: ' + e.currentTarget.getAttribute('data-name');
        }

        render() {
            var html = `<h2>Componente</h2>
                        <div class="row">`;
            data.products.forEach(element => {
                html += `<div class="col s12 m4">
                            <div id="product" data-name="${element.name}" class="card blue-grey lighten-2">
                            <div class="card-content white-text">
                                <span class="card-title">${element.name}</span>
                                <p>${element.desc}</p>
                            </div>
                            <div class="card-action"><a class="black-text" href="${element.url}">Go to product</a></div>
                            </div>
                        </div>`;
            });
            html += `</div>`
            html += `<h6 class="center" id="selected-prod"></h6>`
            this.innerHTML= html;
            this.addListeners();
        }

        addListeners() {
            const $prods = document.querySelectorAll('#product');
            Array.prototype.forEach.call($prods, $prod => {
                $prod.addEventListener('click', this.updateData);
            })
        }

        disconnectedCallback() {
            window.removeEventListener('central:changed', this.refresh);
            this.log('disconnected');
        }

        log(...args) {
            console.log('contenido-central', ...args);
          }
    }

    window.customElements.define('contenido-central', Central);
} ());
