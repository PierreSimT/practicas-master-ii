# Usabilidad. Diseño Plano

Realiza una tabla resumen de las acciones de diseño que previenen los problemas de usabilidad del diseño plano. La entrega se debe hacer mediante enlace a fichero Markdown en el repositorio github de la asignatura y subir una versión en pdf al campus virtual.

Problemas | Acciones 
--- | ---
Comunicar elementos seleccionables | Usar una estética semi plana, mostrar los botones como botones físicos. En general, dar enfásis a elementos que son clickeables.
Eficiencia de navegación por parte del usuario | No usar hover como la única manera de descatar elementos. Añadir énfasis en elementos clickeables.
Fondos con imágenes esconden el resto de elementos | Verificar que el texto es legible y ayuda a comprender el sitio.
Problemas de escala y accesibilidad con tipografías | Usar tipgrafías Web
Pocos elementos en pantalla | Asegurar que cada uno de los elementos presentes contribuyen  a realizar las tareas principales del sitio.