# Práctica Diseño Web Minimalista

Dadas las páginas de los siguientes sitios web, indicar los elementos de diseño web minimalista que incorporan. Realizar un análisis de la usabilidad de los sitios y participar en el foro que sobre el tema.

<http://wmat.it/home>

<http://madebytemple.com/>

<http://paginasweb.staffcreativa.pe/?utm_source=blog&utm_medium=banner&utm_content=end-post&utm_campaign=Blog%20Banners>

## WMAT

### Elementos que Incorpora

![wmat](img/wmat.png)

Los elementos que incorpora del diseño minimalista son los siguientes:

* Es una página pequeña con poco contenido
* El espacio en blanco está totalmente maximizado
* Se puede apreciar el contraste entre el contenido y el fondo
* En cuánto a la interacción, se requiere que el usuario pase el ratón por los elementos de la página
* Si pasas el ratón por el cuadrado, el fondo es un vídeo que ocupa toda la pantalla

### Análisis de Usabilidad

El análisis que se muestra a continuación se realiza siguiendo los principios de usabilidad de Nielsen.

1. Cualquier cambio que se realiza se puede comprobar de forma inmediata.
2. Siempre se ofrece una forma de salir del estado actual al usuario.
3. Uso de un diseño estético y minimalista.
4. Falta de relación entre el sistema y el mundo real
5. No es un diseño que sigue un estándar, la forma de navegar por la página es poco intuitiva respecto a las páginas web actuales.
6. El usuario está forzado a recordar que hace cada botón o letra de navegación.


## MadeByTemple

### Elementos que Incorpora

![wmat](img/madebytemple.png)

Los elementos que incorpora del diseño minimalista son los siguientes:

* Es una página pequeña con poco contenido
* El espacio en blanco está totalmente maximizado
* Se puede apreciar el contraste entre el contenido y el fondo
* Prestando atención a la tipografía, se puede percibir como se le da más importancia a alguno elementos respecto a otros.
* Hace uso de Grids para estructurar múltiples contenidos como se puede apreciar en la página de `Work`.

### Análisis de Usabilidad

El análisis que se muestra a continuación se realiza siguiendo los principios de usabilidad de Nielsen. 

1. Falta de relación entre el sistema y el mundo real.
2. Uso de consistencia y estándares, la página desde un primer instante se sabe como se navega por ella y como se usa.
3. Un fallo leve en visibilidad, cuando cambias de página toma tiempo en cargar, pero el logo de la página no es un buen elemento para indicar que esto está sucediendo.
4. Uso de un diseño estético y minimalista.
5. Uso de un diseño estético y minimalista.



## PaginasWeb.StaffCreativa

### Elementos que incorpora

![wmat](img/staffcrea.png)

Los elementos que incorpora del diseño minimalista son los siguientes:

* Uso de tipografías para dar más importancia a algunos elementos sobre otros.
* Hace uso de patrones y texturas planos, pero con algunos elementos hibrídos para mostrar mejor elemenos clickeables.
* Uso de diferentes colores para crear interés visual
* En algunos elementos de la página se puede apreciar el layout en grid.

### Análisis de Usabilidad

El análisis que se muestra a continuación se realiza siguiendo los principios de usabilidad de Nielsen. 

1. Cuando se realiza click sobre los elementos de navegación la interactividad es básicamente instantánea.
2. Hace uso de una metáfora visual para el formulario de contacto, de esta forma relacionando el sistema y el mundo real.
3. Siempre se ofrece una forma de navegación o control al usuario.
4. Hace uso de un estándar, la barra de navegación, pero esta no es consistente. Si se realiza un scroll los elementos de la barra de navegación cambia.
5. Repetición de elementos, se tiene dos formas de acceder al elemento `proyectos` y al formulario de contacto desde la pantalla inicial de la página. Desde la barra de navegación y desde los botones inferiores.
6. Uso de un diseño estético y minimalista.
